import numpy as np
from numpy import ndarray
import os
from ase.io import read, iread
from ase.neighborlist import build_neighbor_list
from ase import Atoms
from itertools import combinations_with_replacement
from tqdm import tqdm_notebook
import sys
import matplotlib.pyplot as plt
from numpy.linalg import norm
import time
import pickle
import multiprocessing as mp

def read_poscars(pfile, step=1):
    structures = []
    counter = 0
    with open(pfile, 'r') as f:
        lines = f.readlines()
        for i, line in enumerate(lines):
            if 'EA' in line:
                if counter % step == 0:
                    current_struct = []
                    num_of_atoms = sum([int(item) for item in lines[i+6].split()])
                    for line in lines[i:i+8+num_of_atoms+1]:
                        current_struct.append(line)
                    coef = float(current_struct[1])
                    cell = np.ndarray(shape=(3,3))
                    species = current_struct[5].split()
                    num_species = [int(item) for item in current_struct[6].split()]
                    symbols = np.repeat(species, num_species)
                    if 'Selective Dynamics' in current_struct:
                        raise NotImplementedError
                    positions = np.ndarray(shape=(num_of_atoms,3))
                    for j in range(3):
                        cell[j] = np.asarray(current_struct[j+2].split(), dtype=float)
                    cell *= coef
                    for n in range(num_of_atoms):
                        positions[n] = np.asarray(current_struct[n+8].split(), dtype=float)
                    if 'Direct' in current_struct[7]:
                        positions = np.dot(positions, cell)
                    ID = int(current_struct[0].split('EA')[-1])
                    structure = Atoms(symbols=symbols, positions=positions, cell=cell, pbc=True)
                    structure.ID = ID
                    structures.append(structure)
                counter += 1
    return structures

#G4
def get_G4(iStep, struct):
    NamesG4 = []
    LambdaArray = [1]
    RcArray = [5] #[5, 7]
    EtaArray = [0.1] #EtaArray = [2.0, 0.5, 0.1]
    ZetaArray = [10] #[1, 15] #ZetaArray = [1, 5, 10, 50]
    n1 = build_neighbor_list(struct, cutoffs = len(struct.positions)*[np.max(RcArray)/2], bothways=True, self_interaction=False, skin=0.0)
    Features = []
    for iLambda in LambdaArray:
        for iRc in RcArray:
            for iEta in EtaArray:
                for iZeta in ZetaArray:
                    G4iArray = []
                    for iAtom in range(len(struct.positions)):
                        indices, offsets = n1.get_neighbors(iAtom)
                        atomPos = struct.positions[iAtom]
                        neighPos = struct.positions[indices] + offsets @ struct.get_cell()
                        RDist = neighPos-atomPos
                        G4i = 0
                        for j in range(len(RDist)):
                            for k in range(len(RDist)):
                                if k != j:
                                    jVec = RDist[j]
                                    kVec = RDist[k]
                                    pVec = jVec - kVec
                                    fcj = 0.5*(np.cos(np.pi*norm(jVec)/(iRc))+1)
                                    fck = 0.5*(np.cos(np.pi*norm(kVec)/(iRc))+1)
                                    fcp = 0.5*(np.cos(np.pi*norm(pVec)/(iRc))+1)
                                    cosTheta = jVec.dot(kVec)/(norm(jVec)*norm(kVec))
                                    G4i_jk = 2**(1-iZeta)*(1 + iLambda*cosTheta)**iZeta * np.exp(-iEta*(norm(jVec)**2+norm(kVec)**2+norm(pVec)**2)) * fcj*fck*fcp
                                    G4i += G4i_jk
                        G4iArray.append(G4i)
                    if iStep == 0:
                        name = 'l =%i, rc=%i, eta=%.1f, z=%i'%(iLambda, iRc, iEta, iZeta)
                        NamesG4.append(name)
                    G4_OneStruct = np.sum(G4iArray)
                    Features.append(G4_OneStruct)
    print(iStep, 'is done!')
    return iStep, Features, NamesG4

poscars = './POSCARS'
energies = np.loadtxt('./Energies_TOTEN', dtype=float)
structures = read_poscars(poscars)

CalcArray = [(i, struct) for i, struct in enumerate(structures)]
startTime = time.time()
pool = mp.Pool(mp.cpu_count())
out = pool.starmap(get_G4, CalcArray)
print('Total time: %.1fs'%(time.time()-startTime))

G4 = [[] for i in range(len(out))]
NamesG4 = []
for i, iArr, iNames in out:
    G4[i] = iArr
    if len(iNames) >= 1: NamesG4 = iNames
G4 = np.array(G4)

with open('G4_001.pickle', 'wb') as f:
    pickle.dump(G4, f)
with open('G4Names_001.pickle', 'wb') as f:
    pickle.dump(NamesG4, f)
